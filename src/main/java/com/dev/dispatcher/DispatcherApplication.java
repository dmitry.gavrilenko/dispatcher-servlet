package com.dev.dispatcher;

import com.dev.dispatcher.config.RootConfig;
import org.springframework.boot.SpringApplication;

public class DispatcherApplication {

    public static void main(String[] args) {
        SpringApplication.run(RootConfig.class, args);
    }

}
